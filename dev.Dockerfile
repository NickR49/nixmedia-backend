FROM node:12.8-alpine

RUN mkdir -p /app
WORKDIR /app

COPY package*.json ./
RUN npm install
RUN npm install -g typescript --unsafe-perm
RUN npm install -g prisma --unsafe-perm

COPY . .

EXPOSE 4445
CMD ["npm", "run", "tsdev"]

