FROM node:12.8-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm install -g typescript --unsafe-perm
RUN npm install -g prisma --unsafe-perm

# Do TypeScript compilation
COPY ./src ./src
COPY tsconfig.json .
RUN tsc

# Copy Prisma files
COPY ./prisma.yml .
COPY ./*.prisma ./
COPY ./.graphqlconfig.yml .

EXPOSE 4445
CMD ["npm", "run", "prod"]

