# Nix Media

## Backend

This backend wraps the Movies REST API endpoint within a GraphQL Resolver and provides user authentication.

### Technologies

 - TypeScript
 - Apollo Server
 - Yoga
 - Prisma
 - Docker

### Build

The backend commits trigger BitBucket pipelines which builds the app, builds a Docker image and uploads it to GCR. 

### API

Use port 4445 to access GraphQL playground -


```
query {
  movies {
     entries {
      id
      title
      description
      contents {
        url
      }
      images {
        url
      }
    }
  }
}
```
