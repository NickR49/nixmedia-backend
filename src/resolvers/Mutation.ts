import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
// import { randomBytes } from 'bcryptjs';
// const _ = require("lodash");

import * as tileUtils from '../utils/tileUtils';
import { transport, makeANiceEmail } from '../utils/mail';
import * as dict from '../services/dictService';
import { setBoardLayout } from '../utils/boardLayout';
import {
  Tile,
  BoardTile,
  Coord,
  Orientation,
  TileState,
  BoardWord,
  MoveType,
} from './../types';

const Mutations = {
  /*
   * Signup
   */
  async signup(parent: any, args: any, ctx: any, info: any) {
    // lowercase their email
    args.email = args.email.toLowerCase();
    // hash their password
    const password = await bcrypt.hash(args.password, 10);
    // create the user in the database
    let user;
    try {
      user = await ctx.db.mutation.createUser({
        data: {
          ...args,
          password,
          permissions: { set: ['USER'] },
        },
      });
    } catch (err) {
      // TODO Display a decent error for email unique constraint violation
      console.log('Signup error: ', JSON.stringify(err));
      // throw new Error("That email is already taken.");
      throw new Error(err);
    }
    // create the JWT token for the user
    const appSecret = process.env.APP_SECRET;
    // temp fudge
    if (appSecret) {
      const token = jwt.sign({ userId: user.id }, appSecret);

      return {
        token,
        user,
      };
    }
  },
  /*
   * Signout
   */
  async signout(parent: any, args: any, ctx: any, info: any) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }

    // Update the user status
    const user = await ctx.db.mutation.updateUser({
      data: {
        loggedIn: false,
      },
      where: {
        id: ctx.request.userId,
      },
    });
    return { message: 'Goodbye!' };
  },
  /*
   * Signin
   */
  async signin(
    parent: any,
    { email, password }: { email: string; password: string },
    ctx: any,
    info: any
  ) {
    // 1. check if there is a user with that email
    const user = await ctx.db.query.user({ where: { email } });
    if (!user) {
      throw new Error(`No such user found for email ${email}`);
      // throw new AuthenticationError(`No such user found for email ${email}`);  // TODO Apollo server feature
    }
    // 2. check if their password is correct
    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error('Invalid password');
      // throw new AuthenticationError("Invalid password"); // TODO Apollo server feature
    }
    // 3. update the user status
    await ctx.db.mutation.updateUser({
      data: {
        loggedIn: true,
      },
      where: {
        id: user.id,
      },
    });

    // 4. generate the JWT token
    const appSecret = process.env.APP_SECRET;
    // temp fudge
    if (appSecret) {
      const token = jwt.sign({ userId: user.id }, appSecret);
      // 5. return the token and user
      return {
        token,
        user,
      };
    }
  },

  /*
   * watchMovie
   */
  async watchMovie(parent: any, args: any, ctx: any, info: any) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }
    const id = await ctx.db.mutation.createUser_Movie({
      data: {
        movieId: args.id,
        user: {
          connect: {
            id: ctx.request.userId,
          },
        },
        lastWatched: Math.floor(new Date().getTime() / 1000),
      },
    });
    return id;
  },

  /*
   * initGame
   */
  async createGame(
    parent: any,
    { users }: { users: string[] },
    ctx: any,
    info: any
  ) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }
    // Create the game
    // const game = await ctx.db.mutation.createGame({
    //   data: {
    //     players: {
    //       create: [
    //         {
    //           user: {
    //             connect: {
    //               id: args.users[0],
    //             },
    //           },
    //         },
    //         {
    //           user: {
    //             connect: {
    //               id: args.users[1],
    //             },
    //           },
    //         },
    //       ],
    //     },
    //   },
    // });

    // Add the authenticated user to the beginning of the users array
    users = [ctx.request.userId, ...users];

    // Check that we have 2 - 4 players
    if (users.length < 2)
      throw new Error('At least two players are required for a game');
    if (users.length > 4)
      throw new Error('Four players is the maximum allowed');

    // Set the game bag
    let bagTiles = tileUtils.getBagTiles();

    // Use array of user ids to create an array of user_games
    let playerNumber = 0;
    const user_games = users.map(userId => {
      // Set the player tiles
      let playerTiles = tileUtils.pickTilesFromBag(bagTiles, [], 7);
      return {
        user: {
          connect: {
            id: userId,
          },
        },
        rack: {
          create: playerTiles,
        },
        score: 0,
        playerNumber: playerNumber++,
      };
    });

    // Set the board (note that lodash fill creates 225 tiles of the same object - modifying one object modifies all objects)
    // const row = _.fill(Array(15), { tileId: null, letter: null, value: null });
    // const rows = _.fill(Array(15), { row: { create: row } });
    let rows = [];
    for (let i = 0; i < 15; i++) {
      let row = [];
      for (let j = 0; j < 15; j++) {
        row.push({ tileId: null, letter: null, value: null });
      }
      rows.push({ row: { create: row } });
    }

    // TODO Populate board layout from DB
    const board = setBoardLayout(rows);

    const game = await ctx.db.mutation.createGame({
      data: {
        players: {
          create: user_games,
        },
        bag: {
          create: bagTiles,
        },
        board: {
          create: board,
        },
        currentPlayerNumber: 0,
        currentRoundNumber: 0,
        rounds: {
          create: [{ roundNumber: 0 }],
        },
      },
    });

    return game;
  },
  /*
   * Submit Wordplay Move
   */
  async submitMove(
    parent: any,
    {
      gameId,
      moveType,
      tiles,
    }: { gameId: string; moveType: string; tiles: BoardTile[] },
    ctx: any,
    info: any
  ) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }
    console.log('gameId: ', gameId);
    console.log('MoveType: ', moveType);
    console.log('Tiles: ', JSON.stringify(tiles));

    // Handle forfeits
    if (moveType === MoveType.FORFEIT) {
    }
    // Handle exchanges
    if (moveType === MoveType.EXCHANGE) {
    }
    // Handle passes
    if (moveType === MoveType.PASS) {
    }

    // Load the game data
    const game = await ctx.db.query.game(
      {
        where: { id: gameId },
      },
      `{
        id
        players {
          id
          user {
            id
            name
          }
          playerNumber
          score
        }
        board {
          row {
            tileId
            letter
            value
            bonus
          }
        }
        currentPlayerNumber
        currentRoundNumber
        rounds {
          roundNumber
          moves {
            playerNumber
            moveType
            tiles {
              tileId
              letter
              value
            }
            points
            score
          }
        }
      }`
    );
    // ------------------------------------------------------------------------------------
    // Check that the correct player is making this move
    // ------------------------------------------------------------------------------------
    // Get the authenticated user's player number
    //TODO Fix up player : any
    let thisMovePlayer = game.players.filter(
      (player: any) => player.user.id === ctx.request.userId
    )[0];
    if (
      thisMovePlayer === null ||
      thisMovePlayer.playerNumber !== game.currentPlayerNumber
    ) {
      // TODO Log a security event
      throw new Error('It is not your turn');
    }
    // ------------------------------------------------------------------------------------
    // Check that letters submitted are a subset of the player's actual current tiles
    // ------------------------------------------------------------------------------------
    if (false) {
      // TODO Log a security event
      throw new Error("These are not the tiles you're looking for");
    }
    // ------------------------------------------------------------------------------------
    // Check that the tiles are all in a line
    // ------------------------------------------------------------------------------------
    const allXsEqual = tiles.every(tile => tile.x === tiles[0].x);
    const allYsEqual = tiles.every(tile => tile.y === tiles[0].y);
    if (!allXsEqual && !allYsEqual) {
      throw new Error('The played tiles must all be on the same line');
    }
    let orientation: Orientation;
    if (allYsEqual) {
      orientation = Orientation.HORIZONTAL;
    } else {
      orientation = Orientation.VERTICAL;
    }

    // ------------------------------------------------------------------------------------
    // Is this the first played move? ie. not a pass, exchange or forfeit
    // ------------------------------------------------------------------------------------
    let firstPlayedMove = true;
    game.rounds.forEach((round: any) => {
      round.moves.forEach((move: any) => {
        if (move.moveType === MoveType.PLAY) {
          firstPlayedMove = false;
        }
      });
    });
    // ------------------------------------------------------------------------------------
    // If first played move, check that letters overlap the star
    // ------------------------------------------------------------------------------------
    if (firstPlayedMove) {
      let letterOnStar = false;
      tiles.forEach(tile => {
        if (tile.x === 7 && tile.y === 7) {
          letterOnStar = true;
        }
      });
      if (!letterOnStar) {
        throw new Error('The first move must be played across the star');
      }
    }
    // ------------------------------------------------------------------------------------
    // Load the tiles onto the board
    // ------------------------------------------------------------------------------------
    tiles.forEach(tile => {
      const boardTile = game.board[tile.y].row[tile.x];
      // Check if there is a board tile already where the player is trying to play a tile
      if (boardTile.tileId !== null) {
        // TODO Log a security event
        throw new Error('There is already a tile there!');
      }
      boardTile.tileId = tile.tileId;
      boardTile.letter = tile.letter;
      boardTile.value = tile.value;
      boardTile.state = TileState.TILE_LOOSE_ON_BOARD;
    });
    // ------------------------------------------------------------------------------------
    // Check there are no gaps in the played move
    // ------------------------------------------------------------------------------------
    if (orientation === Orientation.HORIZONTAL) {
      // Check all tile positions from first tile letter to last tile letter for any gaps
      const y = tiles[0].y;
      for (let x = tiles[0].x; x <= tiles[tiles.length - 1].x; x++) {
        if (game.board[y].row[x].tileId === null) {
          throw new Error('There can be no gaps in the letters played');
        }
      }
    }
    if (orientation === Orientation.VERTICAL) {
      // Check all tile positions from first tile letter to last tile letter for any gaps
      const x = tiles[0].x;
      for (let y = tiles[0].y; y <= tiles[tiles.length - 1].y; y++) {
        if (game.board[y].row[x].tileId === null) {
          throw new Error('There can be no gaps in the letters played');
        }
      }
    }

    // ------------------------------------------------------------------------------------
    // Check none of the letters have been played where there are existing board letters
    // ------------------------------------------------------------------------------------
    if (false) {
      // TODO Log a security event
      throw new Error('Invalid move - there is already a letter there');
    }
    // ------------------------------------------------------------------------------------
    // Check the word has been played adjacent to existing tiles
    // ------------------------------------------------------------------------------------
    // board {
    //   row {
    //     tileId
    //   }
    // }
    // board is an array of rows
    // row is an array of tiles
    // Check that it's not the first played move
    if (!firstPlayedMove) {
      let adjacentBoardTile = false;
      tiles.forEach(tile => {
        // Check all around the current tile to see if there are any adjacent board tiles
        // Left
        if (tile.x !== 0 && game.board[tile.y].row[tile.x - 1]) {
          adjacentBoardTile = true;
        }
        // Right
        if (tile.x !== 14 && game.board[tile.y].row[tile.x + 1]) {
          adjacentBoardTile = true;
        }
        // Above
        if (tile.y !== 0 && game.board[tile.y - 1].row[tile.x]) {
          adjacentBoardTile = true;
        }
        // Below
        if (tile.y !== 14 && game.board[tile.y + 1].row[tile.x]) {
          adjacentBoardTile = true;
        }
      });

      if (!adjacentBoardTile) {
        throw new Error(
          'At least one of the played tiles must be adjacent to an existing tile'
        );
      }
    }
    // ------------------------------------------------------------------------------------
    // Generate a list of all the words that have been formed
    // ------------------------------------------------------------------------------------
    const wordList = tileUtils.wordList(game.board, tiles, orientation);
    console.log('Word List: ', JSON.stringify(wordList));

    // ------------------------------------------------------------------------------------
    // Check that all words formed are valid in the dictionary
    // ------------------------------------------------------------------------------------
    let invalidWords: string[] = [];
    await wordList.forEach(async (wordEntry: BoardWord) => {
      // try {
      const response = await dict
        .getWord(wordEntry.word)
        .catch(function(error: any) {
          if (error.response && error.response.status === 404) {
            invalidWords.push(wordEntry.word.toUpperCase());
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Something else happened');
          }
        });
    });
    if (invalidWords.length === 1) {
      throw new Error(invalidWords[0] + ' is not a valid word');
    }
    if (invalidWords.length > 1) {
      throw new Error(invalidWords + ' are not valid words');
    }
    console.log('invalidWords.length: ', invalidWords.length);

    // ------------------------------------------------------------------------------------
    // If the tiles passed the above checks then it must be a valid move
    // ------------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------------
    // Calculate the score
    // ------------------------------------------------------------------------------------
    let totalScore = 0;
    wordList.forEach(
      async ({
        startCoord,
        endCoord,
      }: {
        startCoord: Coord;
        endCoord: Coord;
      }) => {
        // Vertical word
        if (startCoord.x === endCoord.x) {
          let multiplier = 1;
          let wordScore = 0;
          let x = startCoord.x;
          for (let y = startCoord.y; y <= endCoord.y; y++) {
            let tile = game.board[y].row[x];
            let playerTile = tile.state === TileState.TILE_LOOSE_ON_BOARD;
            if (playerTile && tile.bonus === 'DW') multiplier *= 2;
            if (playerTile && tile.bonus === 'TW') multiplier *= 3;
            let letterScore = tile.value;
            if (playerTile && tile.bonus === 'DL') letterScore *= 2;
            if (playerTile && tile.bonus === 'TL') letterScore *= 3;
            wordScore += letterScore;
            console.log(`letterScore for ${tile.letter}: ${letterScore}`);
          }
          wordScore *= multiplier;
          console.log('wordScore: ', wordScore);
          totalScore += wordScore;
        }
        // Horizontal word
        if (startCoord.y === endCoord.y) {
          let multiplier = 1;
          let wordScore = 0;
          let y = startCoord.y;
          for (let x = startCoord.x; x <= endCoord.x; x++) {
            let tile = game.board[y].row[x];
            let playerTile = tile.state === TileState.TILE_LOOSE_ON_BOARD;
            if (playerTile && tile.bonus === 'DW') multiplier *= 2;
            if (playerTile && tile.bonus === 'TW') multiplier *= 3;
            let letterScore = tile.value;
            if (playerTile && tile.bonus === 'DL') letterScore *= 2;
            if (playerTile && tile.bonus === 'TL') letterScore *= 3;
            wordScore += letterScore;
            console.log(`letterScore for ${tile.letter}: ${letterScore}`);
          }
          wordScore *= multiplier;
          console.log('wordScore: ', wordScore);
          totalScore += wordScore;
        }
      }
    );
    //TODO Add 50 points if all seven letters used

    console.log('totalScore: ', totalScore);

    // Perform updates
    // Game - record the move, update board, bag, current player number, current round (if necessary)

    //  Record the move (Update game, update round, create move)

    // await ctx.db.mutation.updateGame({
    //   data: {
    //     loggedIn: true,
    //   },
    //   where: {
    //     id: user.id,
    //   },
    // });

    // User_Game - update score, rack (remove tiles, add tiles)

    // Round - create new round if necessary

    return null;
  },
  /*
   * Waiting on Prisma to implement cascading deletes for MongoDB connector.
   * In the meantime manually delete related user_game documents.
   */
  async deleteGame(parent: any, { id }: { id: string }, ctx: any, info: any) {
    // if (!ctx.request.userId) {
    //   throw new Error("You must be logged in to do that!");
    // }
    // TODO: Query on all related user_game documents
    // const userGames = await ctx.db.query.user_games
    const game = await ctx.db.mutation.deleteGame({ id }, info);
    return game;
  },
  /*
   *
   */
  async createFriend(
    parent: any,
    { userId }: { userId: string },
    ctx: any,
    info: any
  ) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }

    const friend = await ctx.db.mutation.createFriend({
      data: {
        friends: {
          connect: [
            {
              id: ctx.request.userId,
            },
            {
              id: userId,
            },
          ],
        },
      },
    });

    return friend;
  },
  /*
   *
   */
  async createInvite(
    parent: any,
    {
      inviteeName,
      inviteeEmail,
    }: { inviteeName: string; inviteeEmail: string },
    ctx: any,
    info: any
  ) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }

    // TODO validate email

    const invite = await ctx.db.mutation.createInvite({
      data: {
        inviterId: {
          connect: {
            id: ctx.request.userId,
          },
        },
        inviteeName,
        inviteeEmail,
        status: 'open',
      },
    });

    // Send an email . . .
    const mailResponse = await transport.sendMail({
      from: `${process.env.MAIL_FROM}`,
      to: inviteeEmail,
      subject: 'Wordplay Invite',
      html: makeANiceEmail(
        `Your friend ${ctx.request.user.name} is inviting you to play Wordplay!
        <br />
        <a href='${process.env.FRONTEND_URL}/invite/${invite.id}'>
          Click Here to Signup
        </a>`
      ),
    });

    return invite;
  },
  /*
   *
   */
  // async updateInvite(
  //   parent,
  //   { inviteeId, inviteeName, inviteeEmail, status },
  //   ctx,
  //   info
  // ) {
  //   if (!ctx.request.userId) {
  //     throw new Error("You must be logged in to do that!");
  //   }

  //   // TODO validate email

  //   const invite = await ctx.db.mutation.updateInvite({
  //     data: {
  //       inviterId: {
  //         connect: {
  //           id: ctx.request.userId,
  //         },
  //       },
  //       inviteeName,
  //       inviteeEmail,
  //       status: "open",
  //     },
  //   });

  //   return invite;
  // },
  async deleteInvite(parent: any, { id }: { id: string }, ctx: any, info: any) {
    if (!ctx.request.userId) {
      throw new Error('You must be logged in to do that!');
    }
    return await ctx.db.mutation.deleteInvite({ id }, info);
  },
};

export default Mutations;
