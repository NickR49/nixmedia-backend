// const { forwardTo } = require('prisma-binding');
import { forwardTo } from 'prisma-binding';
import fetch from 'node-fetch';

const baseURL = `https://demo2697834.mockable.io/movies`;

const Query = {
  async movies(parent: any, args: any, ctx: any, info: any) {
    const movies = await fetch(`${baseURL}`).then((res: any) => res.json());
    // console.log(`movies: `, movies);
    return movies;
  },
  // async movieImage(parent: any, args: any, ctx: any, info: any) {
  //   if (!ctx.request.userId) {
  //     return null;
  //   }
  //   const image = await fetch(args.url).then((res: any) => res.blob());
  //   return image;
  // },
  users: forwardTo('db'),
  me(parent: any, args: any, ctx: any, info: any) {
    // check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }
    const user = ctx.db.query.user(
      {
        where: { id: ctx.request.userId },
      },
      info
    );
    return user;
  },
  game(parent: any, args: any, ctx: any, info: any) {
    // check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }
    // console.log("args: ", args);

    const game = ctx.db.query.game(
      {
        // where: { id: "5c39a9f6857aba00098bb88c" },
        ...args,
      },
      info
    );

    // Need to add some smarts here so that only the rack of the logged in player is returned,
    // the number of tiles for other player racks is returned,
    // the scores of all players are returned,
    // the tile bag tiles not to be returned, only the number of tiles,
    // the next player whose turn it is
    // Authorization in data models - https://www.apollographql.com/docs/apollo-server/features/authentication.html
    // Factoring out fetching details - https://www.apollographql.com/docs/graphql-tools/connectors.html
    // Aggregation  - https://www.prisma.io/forum/t/how-to-use-field-resolvers-to-get-aggregates-of-inner-relation-types/2930

    return game;
  },
  async games(parent: any, args: any, ctx: any, info: any) {
    // check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }

    const games = await ctx.db.query.games(
      {
        where: { players_some: { user: { id: ctx.request.userId } } },
      },
      info
    );

    return games;
  },
  async friends(parent: any, args: any, ctx: any, info: any) {
    // check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }

    const data = await ctx.db.query.friends(
      {
        where: { friends_some: { id: ctx.request.userId } },
      },
      info
    );
    // console.log("data - ", JSON.stringify(data));

    // Filter out the requesting user from the graph results
    let newFriends: any = [];
    data.forEach((d: any) => {
      //TODO check if f's type is working
      d.friends.forEach((f: { id: any }) => {
        if (f.id !== ctx.request.userId) {
          newFriends.push(f);
        }
      });
    });
    // console.log("newFriends - ", JSON.stringify(newFriends));

    let newData = [{ friends: newFriends }];
    // console.log("newData - ", JSON.stringify(newData));
    return newData;
  },
  async invites(parent: any, args: any, ctx: any, info: any) {
    // check if there is a current user id
    if (!ctx.request.userId) {
      return null;
    }

    const invites = await ctx.db.query.invites(
      {
        where: { inviterId: { id: ctx.request.userId } },
      },
      info
    );
    // console.log("invites - ", JSON.stringify(invites));

    return invites;
  },
  invite(parent: any, args: any, ctx: any, info: any) {
    // Don't check for a current user id

    const invite = ctx.db.query.invite(
      {
        // where: { id: id },
        ...args,
      },
      info
    );
    return invite;
  },
};

export default Query;
