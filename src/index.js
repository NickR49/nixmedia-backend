import jwt from 'jsonwebtoken';
require('dotenv').config({ path: 'variables.env' });
import createServer from './createServer';
import db from './db';

const server = createServer();

// Get the user id
server.express.use((req, res, next) => {
  const header = req.headers.token;
  if (header) {
    const token = header.replace('Bearer ', '');
    try {
      const { userId } = jwt.verify(token, process.env.APP_SECRET);
      req.userId = userId;
    } catch (err) {
      console.log('Encountered error verifying token', token);
      res.status(401).send('Access denied. Invalid token provided.');
      // throw new AuthenticationError("Access denied. Invalid token provided."); // TODO Apollo server feature
      next(err);
    }
  }
  next();
});

// Populate the user on each request
server.express.use(async (req, res, next) => {
  if (!req.userId) return next();
  const user = await db.query.user(
    { where: { id: req.userId } },
    '{id, permissions, email, name}'
  );
  // eslint-disable-next-line require-atomic-updates
  req.user = user;
  next();
});

server.start(
  {
    cors: {
      credentials: true,
      // origin: process.env.FRONTEND_URL,
      origin: '*',
    },
  },
  deets => {
    console.log(`CORS origin is ${process.env.FRONTEND_URL}`);
    console.log(`Server is now running on port http://localhost:${deets.port}`);
  }
);
