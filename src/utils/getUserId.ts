import jwt from 'jsonwebtoken';

const getUserId = (request: any) => {
  const header = request.request.headers.authorization;

  if (!header) {
    throw new Error('Authentication required');
  }

  const token = header.replace('Bearer ', '');
  const decoded: any = jwt.verify(token, 'thisisasecret');

  return decoded.userId;
};

export { getUserId as default };
