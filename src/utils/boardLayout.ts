export const setBoardLayout = (boardSquares: any) => {
  boardSquares[0].row.create[0].bonus = 'TW';
  boardSquares[0].row.create[7].bonus = 'TW';
  boardSquares[0].row.create[14].bonus = 'TW';
  boardSquares[7].row.create[0].bonus = 'TW';
  boardSquares[7].row.create[14].bonus = 'TW';
  boardSquares[14].row.create[0].bonus = 'TW';
  boardSquares[14].row.create[7].bonus = 'TW';
  boardSquares[14].row.create[14].bonus = 'TW';
  boardSquares[1].row.create[1].bonus = 'DW';
  boardSquares[1].row.create[13].bonus = 'DW';
  boardSquares[2].row.create[2].bonus = 'DW';
  boardSquares[2].row.create[12].bonus = 'DW';
  boardSquares[3].row.create[3].bonus = 'DW';
  boardSquares[3].row.create[11].bonus = 'DW';
  boardSquares[4].row.create[4].bonus = 'DW';
  boardSquares[4].row.create[10].bonus = 'DW';
  boardSquares[10].row.create[4].bonus = 'DW';
  boardSquares[10].row.create[10].bonus = 'DW';
  boardSquares[11].row.create[3].bonus = 'DW';
  boardSquares[11].row.create[11].bonus = 'DW';
  boardSquares[12].row.create[2].bonus = 'DW';
  boardSquares[12].row.create[12].bonus = 'DW';
  boardSquares[13].row.create[1].bonus = 'DW';
  boardSquares[13].row.create[13].bonus = 'DW';
  boardSquares[1].row.create[5].bonus = 'TL';
  boardSquares[1].row.create[9].bonus = 'TL';
  boardSquares[5].row.create[1].bonus = 'TL';
  boardSquares[5].row.create[5].bonus = 'TL';
  boardSquares[5].row.create[9].bonus = 'TL';
  boardSquares[5].row.create[13].bonus = 'TL';
  boardSquares[9].row.create[1].bonus = 'TL';
  boardSquares[9].row.create[5].bonus = 'TL';
  boardSquares[9].row.create[9].bonus = 'TL';
  boardSquares[9].row.create[13].bonus = 'TL';
  boardSquares[13].row.create[5].bonus = 'TL';
  boardSquares[13].row.create[9].bonus = 'TL';
  boardSquares[0].row.create[3].bonus = 'DL';
  boardSquares[0].row.create[11].bonus = 'DL';
  boardSquares[2].row.create[6].bonus = 'DL';
  boardSquares[2].row.create[8].bonus = 'DL';
  boardSquares[3].row.create[0].bonus = 'DL';
  boardSquares[3].row.create[7].bonus = 'DL';
  boardSquares[3].row.create[13].bonus = 'DL';
  boardSquares[6].row.create[2].bonus = 'DL';
  boardSquares[6].row.create[6].bonus = 'DL';
  boardSquares[6].row.create[8].bonus = 'DL';
  boardSquares[6].row.create[12].bonus = 'DL';
  boardSquares[7].row.create[3].bonus = 'DL';
  boardSquares[7].row.create[11].bonus = 'DL';
  boardSquares[8].row.create[2].bonus = 'DL';
  boardSquares[8].row.create[6].bonus = 'DL';
  boardSquares[8].row.create[8].bonus = 'DL';
  boardSquares[8].row.create[12].bonus = 'DL';
  boardSquares[11].row.create[0].bonus = 'DL';
  boardSquares[11].row.create[7].bonus = 'DL';
  boardSquares[11].row.create[14].bonus = 'DL';
  boardSquares[12].row.create[6].bonus = 'DL';
  boardSquares[12].row.create[8].bonus = 'DL';
  boardSquares[14].row.create[3].bonus = 'DL';
  boardSquares[14].row.create[11].bonus = 'DL';
  // boardSquares[7].row.create[7].bonus = "*";
  // boardSquares[7].row.create[7].bonus = "\uD83D\uDFD0";
  // boardSquares[7].row.create[7].bonus = "&#x1f7d0";
  // boardSquares[7].row.create[7].bonus = "\u2739";
  boardSquares[7].row.create[7].bonus = 'DW';

  return boardSquares;
};
