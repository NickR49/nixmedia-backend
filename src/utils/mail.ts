import nodemailer from 'nodemailer';

export const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST!,
  port: parseInt(process.env.MAIL_PORT!, 10),
  auth: {
    user: process.env.MAIL_USER!,
    pass: process.env.MAIL_PASSWORD!,
  },
});

export const makeANiceEmail = (text: string) => `
  <div class="email" style="
    border: 1px solid black;
    background-color: lightsteelblue;
    padding: 20px;
    font-family: sans-serif;
    line-height: 2;
    font-size: 20px;
  ">
    <h2 style="padding: 0px; margin: 0px;">Wordplay</h2>
    <p>${text}</p>
    <p>The Wordplay Team</p>
  </div>
`;

// exports.transport = transport;
// exports.makeANiceEmail = makeANiceEmail;
