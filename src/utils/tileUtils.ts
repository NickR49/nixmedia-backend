import { Tile, BoardTile, BoardWord, Orientation, TileState } from './../types';
// import _ from "lodash";
import _ from 'lodash';

// 2 blank tiles (scoring 0 points)
// 1 point: E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4
// 2 points: D ×4, G ×3
// 3 points: B ×2, C ×2, M ×2, P ×2
// 4 points: F ×2, H ×2, V ×2, W ×2, Y ×2
// 5 points: K ×1
// 8 points: J ×1, X ×1
// 10 points: Q ×1, Z ×1

const bagTilesString =
  '  EEEEEEEEEEEEAAAAAAAAAIIIIIIIIIOOOOOOOONNNNNNRRRRRRTTTTTTLLLLSSSSUUUUDDDDGGGBBCCMMPPFFHHVVWWYYKJXQZ';
const bagTilesLettersArray = bagTilesString.split('');

// interface letterValue {
//   letter: string,
//   value: number
// }

interface LetterValues {
  [index: string]: number;
}

const letterValues: LetterValues = {
  ' ': 0,
  A: 1,
  B: 3,
  C: 3,
  D: 2,
  E: 1,
  F: 4,
  G: 2,
  H: 4,
  I: 1,
  J: 8,
  K: 5,
  L: 1,
  M: 3,
  N: 1,
  O: 1,
  P: 3,
  Q: 10,
  R: 1,
  S: 1,
  T: 1,
  U: 1,
  V: 4,
  W: 4,
  X: 8,
  Y: 4,
  Z: 10,
};

export const getBagTiles = () => {
  let bagTiles: Tile[] = [];
  let index = 0;
  bagTilesLettersArray.map(letter => {
    return bagTiles.push({
      tileId: index++,
      letter: letter,
      value: letterValues[letter],
      // state: TILE_IN_BAG,
    });
  });
  // Shuffle the game tiles for some extra randomness
  bagTiles = _.shuffle(bagTiles);

  return bagTiles;
};

export const pickTilesFromBag = (
  bagTiles: Tile[],
  playerTiles: Tile[],
  tilesToGet: number
) => {
  // Pick tiles from the bag
  for (let i = 0; i < tilesToGet; i++) {
    let chosenTileIndex = Math.floor(Math.random() * bagTiles.length);
    let chosenTile = bagTiles.splice(chosenTileIndex, 1)[0];
    // chosenTile.state = TILE_IN_RACK_A;
    playerTiles.push(chosenTile);
  }
  return playerTiles;
};

//TODO Remove board : any
function getHorizontalExtents(board: any, x: number, y: number) {
  // Work out the left extent of the word
  let leftX = x;
  while (true) {
    if (leftX === 0) break;
    leftX--;
    if (board[y].row[leftX].tileId === null) {
      leftX++;
      break;
    }
  }
  // Work out the right extent of the word
  let rightX = x;
  while (true) {
    if (rightX === 15) break;
    rightX++;
    if (board[y].row[rightX].tileId === null) {
      rightX--;
      break;
    }
  }
  return { leftX, rightX };
}

//TODO Remove board : any
function getVerticalExtents(board: any, x: number, y: number) {
  // Work out the top extent of the word
  let topY = y;
  while (true) {
    if (topY === 0) break;
    topY--;
    if (board[topY].row[x].tileId === null) {
      topY++;
      break;
    }
  }
  // Work out the right extent of the word
  let bottomY = y;
  while (true) {
    if (bottomY === 15) break;
    bottomY++;
    if (board[bottomY].row[x].tileId === null) {
      bottomY--;
      break;
    }
  }
  return { topY, bottomY };
}

//TODO Remove board : any
export const wordList = (
  board: any,
  tiles: BoardTile[],
  orientation: Orientation
): BoardWord[] => {
  let wordList: BoardWord[] = [];

  if (orientation === Orientation.HORIZONTAL) {
    const y = tiles[0].y;
    const { leftX, rightX } = getHorizontalExtents(board, tiles[0].x, y);

    let word = '';
    for (let x = leftX; x <= rightX; x++) {
      word = word + board[y].row[x].letter;
    }
    console.log("Primary Horizontal word: '" + word + "'");
    wordList.push({
      // orientation: HORIZONTAL,
      startCoord: {
        x: leftX,
        y,
      },
      endCoord: {
        x: rightX,
        y,
      },
      word,
    });
    // Now find any vertical words that have been made
    tiles.forEach(tile => {
      const { topY, bottomY } = getVerticalExtents(board, tile.x, tile.y);
      // If we have a word (more than one letter) then capture it
      if (bottomY - topY > 0) {
        let word = '';
        for (let y = topY; y <= bottomY; y++) {
          word = word + board[y].row[tile.x].letter;
        }
        console.log("Secondary Vertical word: '" + word + "'");
        wordList.push({
          // orientation: VERTICAL,
          startCoord: {
            x: tile.x,
            y: topY,
          },
          endCoord: {
            x: tile.x,
            y: bottomY,
          },
          word,
        });
      }
    });
  }

  if (orientation === Orientation.VERTICAL) {
    const x = tiles[0].x;
    const { topY, bottomY } = getVerticalExtents(board, x, tiles[0].y);

    let word = '';
    for (let y = topY; y <= bottomY; y++) {
      word = word + board[y].row[x].letter;
    }
    console.log("Primary Vertical word: '" + word + "'");
    wordList.push({
      // orientation: VERTICAL,
      startCoord: {
        x,
        y: topY,
      },
      endCoord: {
        x,
        y: bottomY,
      },
      word,
    });
    // Now find any horizontal words that have been made
    tiles.forEach(tile => {
      const { leftX, rightX } = getHorizontalExtents(board, tile.x, tile.y);
      // If we have a word (more than one letter) then capture it
      if (rightX - leftX > 0) {
        let word = '';
        for (let x = leftX; x <= rightX; x++) {
          word = word + board[tile.y].row[x].letter;
        }
        console.log("Secondary Horizontal word: '" + word + "'");
        wordList.push({
          // orientation: HORIZONTAL,
          startCoord: {
            x: leftX,
            y: tile.y,
          },
          endCoord: {
            x: rightX,
            y: tile.y,
          },
          word,
        });
      }
    });
  }

  return wordList;
};
