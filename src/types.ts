
export interface Tile {
  tileId: number,
  letter: string,
  value: number,
}

export interface BoardTile extends Tile {
  x: number,
  y: number
}

export interface BoardWord {
  startCoord: Coord,
  endCoord: Coord,
  word: string,
}

export interface Coord {
  x: number,
  y: number
}

export enum Orientation {
  HORIZONTAL = 0,
  VERTICAL = 1
}

export enum TileState {
  TILE_IN_BAG = 0,
  TILE_IN_RACK = 1,
  TILE_LOOSE_ON_BOARD = 2,
  TILE_FIXED_ON_BOARD = 3,
}

export enum MoveType {
  PLAY = "PLAY",
  EXCHANGE = "EXCHANGE",
  PASS = "PASS",
  FORFEIT = "FORFEIT"
}