import { get } from './httpService';
// import { apiUrl } from "../config.json";

// https://api.wordnik.com/v4/word.json/dolphin/scrabbleScore?api_key=[API_KEY]

const apiEndpoint = '/scrabbleScore';
const api_key = process.env.DICT_API_KEY;

function dictUrl(word: string) {
  const url = `/${word}${apiEndpoint}/?api_key=${api_key}`;
  console.log('Dict URL: ', url);
  return url;
}

export const getWord = (word: string) => {
  console.log('getWord: ', word);
  return get(dictUrl(word.toLowerCase()));
};
