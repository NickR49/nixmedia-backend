import axios from 'axios';

axios.defaults.baseURL = process.env.DICT_API_URL;

// TODO Add Sentry logging
// axios.interceptors.response.use(null, error => {
//   const expectedError =
//     error.response &&
//     error.response.status >= 400 &&
//     error.response.status < 500;

//   if (expectedError) {
//     console.log("Expected error: ", error);
//   } else {
//     // Sentry.captureException(error);
//     console.log("Unexpected error: ", error);
//   }
//   return Promise.reject(error);
// });

// exports.get = (req: string) => {
//   return axios.get(req);
// };

export const get = (req: string) => {
  return axios.get(req);
};
